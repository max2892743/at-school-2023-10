import model.Kotik;

public class Application {
    public static  void main(String[] args) {
        Kotik catOne = new Kotik(2, "Dana", 30, "Meow");
        Kotik catTwo = new Kotik();
        catTwo.setKotik(5, "Roxy", 20, "Moo");

        catTwo.liveAnotherDay();

        System.out.println();
        System.out.println(catTwo.getName());
        System.out.println(catTwo.getWeight());
        System.out.println(catTwo.getMeow().equals(catOne.getMeow()));
        System.out.println(Kotik.getClassInstanceCounter());
    }
}
