package model;

public class Kotik {
    private static int classInstanceCounter = 0;

    private int satiety = 100;

    private int prettiness;
    private String name;
    private int weight;
    private String meow;

    public Kotik() {
        classInstanceCounter++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        classInstanceCounter++;
    }

    private void catHungry() {
        System.out.println("Cat asks for food!");
    }

    private boolean isHungry() {
        return satiety <= 0;
    }

    public boolean play() {
        if (isHungry()) {
            catHungry();
            return false;
        } else {
            System.out.println("Cat play.");
            satiety -= 10;
            return true;
        }
    }

    public boolean sleep() {
        if (isHungry()) {
            catHungry();
            return false;
        } else {
            System.out.println("Cat sleep.");
            satiety -= 50;
            return true;
        }
    }

    public boolean chaseMouse() {
        if (isHungry()) {
            catHungry();
            return false;
        } else {
            System.out.println("Cat chase mouse.");
            satiety -= 20;
            return true;
        }
    }

    public boolean speak() {
        if (isHungry()) {
            catHungry();
            return false;
        } else {
            System.out.println("Cat speak(" + meow + ").");
            satiety -= 5;
            return true;
        }
    }

    public void eat(int foodValue) {
        System.out.println("Cat eat.");
        satiety += foodValue;
    }

    public void eat(int foodValue, String foodName) {
        System.out.println("Cat eat(" + foodName + ").");
        satiety += foodValue;
    }

    public void eat() {
        eat(50, "Something");
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            int catAction = (int) (Math.random() * 5 + 1);
            if (isHungry())
                eat();
            else
                switch (catAction) {
                    case 1 -> play();
                    case 2 -> sleep();
                    case 3 -> chaseMouse();
                    case 4 -> speak();
                    case 5 -> eat();
                    default -> System.out.println("Error number method");
                }
        }
    }

    public static int getClassInstanceCounter() {
        return classInstanceCounter;
    }

    public int getSatiety() {
        return satiety;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }
}
